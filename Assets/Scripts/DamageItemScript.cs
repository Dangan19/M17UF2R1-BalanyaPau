﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageItemScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        IDamagable damagable = other.gameObject.GetComponent<IDamagable>();

        if (damagable != null)
        {
            Debug.Log("Collision with damagable item");

            damagable.Damage(10);
        }
    }
}
