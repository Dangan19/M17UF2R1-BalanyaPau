﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyIAController : MonoBehaviour
{    
    private NavMeshAgent enemy;
    private bool isPatrolling;

    [SerializeField] private GameObject player;
    [SerializeField] protected Animator ac;
    [SerializeField] private Transform stopPoint;

    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponent<NavMeshAgent>();
        //player = GameObject.Find("Player").GetComponent<Transform>();
        isPatrolling = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            isPatrolling = !isPatrolling;
        }

        if (isPatrolling)
        {            
            enemy.SetDestination(stopPoint.position);
        }
        else
        {
            enemy.SetDestination(player.transform.position);
        }

        if (enemy.remainingDistance >= enemy.stoppingDistance)
        {
            ac.SetFloat("MoveSpeed", 1);
        }
        else
        {
            ac.SetFloat("MoveSpeed", 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {            
            ac.SetTrigger("Attack");
        }
    }
}
