﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateMachineIAController : MonoBehaviour
{
    private NavMeshAgent enemy;
    private GameObject objective;
    [SerializeField] private float attackColdown;
    [SerializeField] private float idleColdown;
    [SerializeField] protected Animator ac;

    //Patrolling
    [SerializeField] private Transform[] patrollingPoints;
    private int patrollingIndex = 0;

 
    public enum states
    {
        IDLE,
        PATROLLING,
        PERSECUTE,
        ATACK
    }

    public states currentState;

    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponent<NavMeshAgent>();
        currentState = states.IDLE;
        //GotoNextPoint();
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case states.IDLE:
                //Debug.Log("IDLE");
                Idle();
                break;

            case states.PATROLLING:
                //Debug.Log("PATROLLING");
                Patrolling();
                break;

            case states.PERSECUTE:
                //Debug.Log("PERSECUTE");
                Persecute();
                break;

            case states.ATACK:
                //Debug.Log("ATACK");
                Attack();
                break;
        }
    }

    private void Idle()
    {
        idleColdown -= Time.deltaTime;
        ac.SetFloat("MoveSpeed", 0f);

        if (idleColdown < 0)
        {
            currentState = states.PATROLLING;
        }
    }

    private void Patrolling()
    {
        ac.SetFloat("MoveSpeed", 0.5f);
        enemy.speed = 2.5f;

        if (!enemy.pathPending && enemy.remainingDistance < 0.5f)
            GotoNextPoint();
    }

    private void Persecute()
    {
        ac.SetFloat("MoveSpeed", 1f);
        enemy.speed = 3.5f;
        ac.ResetTrigger("Attack");

        if (objective != null)
        {
            enemy.SetDestination(objective.transform.position);            
        }
    }

    private void Attack()
    {
        attackColdown -= Time.deltaTime;

        if (attackColdown < 0)
        {
            ac.SetTrigger("Attack");
            Debug.Log("I atack");
        }
    }

    private void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (patrollingPoints.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        enemy.destination = patrollingPoints[patrollingIndex].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        patrollingIndex = (patrollingIndex + 1) % patrollingPoints.Length;
    }

    public void CollisionInSon(GameObject hit)
    {
        Debug.Log("ZOMBIE SAW THE PLAYER"); 
        objective = hit;
        currentState = states.PERSECUTE;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            currentState = states.ATACK;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            currentState = states.PERSECUTE;
        }
    }
}
