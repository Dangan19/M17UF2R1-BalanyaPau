﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableScriptKey : MonoBehaviour, IPickable
{
    [SerializeField] private DoorScript ds;
    [SerializeField] private GameObject item;

    public void OnPick()
    {
        AudioSource audiosource = gameObject.GetComponent<AudioSource>();
        audiosource.Play();
        Debug.Log("key iteam picked");
        ds.OpenDoor();
        item.SetActive(true);
        Destroy(gameObject);
    }
}
