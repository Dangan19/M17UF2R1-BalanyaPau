﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableScript : MonoBehaviour, IPickable
{
    [SerializeField] GameObject item;

    public void OnPick()
    {
        AudioSource audiosource = gameObject.GetComponent<AudioSource>();
        audiosource.Play();
        Debug.Log("Item picked");
        item.SetActive(true);
        Destroy(gameObject);
    }
}
