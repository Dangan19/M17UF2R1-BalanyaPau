﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealthScript : MonoBehaviour, IDamagable
{
    public int playerHealth;

    public void Damage(int damage)
    {
        playerHealth -= damage;

        if (playerHealth <= 0)
        {
            Death();
        }

    }

    private void Death()
    {
        Debug.Log("The player is dead");
        Destroy(gameObject);
        SceneManager.LoadScene("GameOverLose");
    }

}
