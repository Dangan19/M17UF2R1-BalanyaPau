﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private bool isRunning;
    private Vector3 move;
    [SerializeField] private float playerSpeed;
    [SerializeField] private float jumpHeight;
    private float gravityValue = -9.81f;
    private Animator an;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    public Transform cam;
    public GameObject followTransform;
    public float rotationPower = 3f;
    public Vector2 _look;


    // Start is called before the first frame update
    void Start()
    {
        an = gameObject.GetComponent<Animator>();
        controller = gameObject.GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        groundedPlayer = controller.isGrounded;

        move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;

        #region Vertical Rotation
        _look = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        followTransform.transform.rotation *= Quaternion.AngleAxis(_look.y * rotationPower, Vector3.left);
        followTransform.transform.rotation *= Quaternion.AngleAxis(_look.x * rotationPower, Vector3.up);

        var angles = followTransform.transform.localEulerAngles;
        angles.z = 0;

        var angle = followTransform.transform.localEulerAngles.x;

        //Clamp the Up/Down rotation
        if (angle > 180 && angle < 340)
        {
            angles.x = 340;
        }
        else if (angle < 180 && angle > 40)
        {
            angles.x = 40;
        }

        followTransform.transform.localEulerAngles = angles;
        #endregion

        if (move.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(move.x, move.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            //float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            //transform.rotation = Quaternion.Euler(0f, angle, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            controller.Move(moveDir.normalized * Time.deltaTime * playerSpeed);
        }


        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;

        }
        else
        {
            an.SetBool("Jumping", false);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            playerSpeed = 4;
        }
        else
        {
            playerSpeed = 2;
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            an.SetBool("Crouching", true);
            playerSpeed = 1;
        }
        else
        {
            an.SetBool("Crouching", false);
            
        }
        


        // Changes the height position of the player..
        if (Input.GetKey(KeyCode.Space) && groundedPlayer)
        {
            jumping();            
        }                
        

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);        
        //Debug.Log(move.magnitude * playerSpeed);
        an.SetFloat("velocity", move.magnitude * playerSpeed);

        //Set the player rotation based on the look transform
        transform.rotation = Quaternion.Euler(0, followTransform.transform.rotation.eulerAngles.y, 0);
        //reset the y rotation of the look transform
        followTransform.transform.localEulerAngles = new Vector3(angles.x, 0, 0);

    }

    private void jumping()
    {
        an.SetBool("Jumping", true);
        playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
    }

   
       
    }


    

