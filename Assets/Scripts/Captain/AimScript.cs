﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimScript : MonoBehaviour
{
    private Animator an;
    public GameObject mainCamera;
    public GameObject aimCamera;
    public GameObject reticle;

    private void Start()
    {
        an = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            an.SetBool("aiming", true);
            mainCamera.SetActive(false);
            aimCamera.SetActive(true);

            StartCoroutine(Aiming());
        }
        else
        {
            an.SetBool("aiming", false);
            mainCamera.SetActive(true);
            aimCamera.SetActive(false);
            reticle.SetActive(false);
        }

        if (Input.GetKey(KeyCode.E))
        {
            an.SetBool("pick up", true);
        }
        else
        {
            an.SetBool("pick up", false);
        }
    }

    IEnumerator Aiming()
    {
        yield return new WaitForSeconds(0.25f);
        reticle.SetActive(enabled);
    }
}
