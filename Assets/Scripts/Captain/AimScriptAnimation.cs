﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimScriptAnimation : MonoBehaviour
{
    private Animator an;

    private void Start()
    {
        an = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            an.SetBool("aiming", true);
        }
        else
        {
            an.SetBool("aiming", false);
        }

        if (Input.GetKey(KeyCode.E))
        {
            an.SetBool("pick up", true);
        }
        else
        {
            an.SetBool("pick up", false);
        }
    }

}
