﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CelebrationScript : MonoBehaviour
{
    private Animator an;
    public GameObject mainCamera;
    public CinemachineVirtualCameraBase cinematicCam;
    private MovementController mv;

    // Start is called before the first frame update
    void Start()
    {
        an = gameObject.GetComponent<Animator>();
        mv = gameObject.GetComponent<MovementController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.T))
        {
            an.SetBool("celebration", true);
            //mainCamera.SetActive(false);
            cinematicCam.Priority = 20;
            mv.enabled = false;
            StartCoroutine(Celebration());
        }
        
    }

    IEnumerator Celebration()
    {
        yield return new WaitForSeconds(8f);
        an.SetBool("celebration", false);
        //mainCamera.SetActive(true);
        cinematicCam.Priority = 9;
        mv.enabled = true;
    }
}
