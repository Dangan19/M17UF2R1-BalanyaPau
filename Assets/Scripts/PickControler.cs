﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickControler : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        
        IPickable ipick = other.gameObject.GetComponent<IPickable>();

        if (ipick != null)
        {
            Debug.Log("Can pick" + other.name);

            if (Input.GetKeyDown(KeyCode.E))
            {
                ipick.OnPick();                
            }
        }
    }

    
}
