﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetectionScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {

            StateMachineIAController parent = transform.parent.GetComponent<StateMachineIAController>();
            parent.CollisionInSon(other.gameObject);
        }        
    }
}
